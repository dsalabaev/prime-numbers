<?php

use App\Http\Controllers\PrimeNumberController;
use Illuminate\Support\Facades\Route;

Route::get('/', [PrimeNumberController::class, 'index']);
Route::get('/check', [PrimeNumberController::class, 'check']);
