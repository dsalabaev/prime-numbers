<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class InvalidPrimeNumberException extends Exception
{
    private int $number;

    public function __construct(
        string $message = "",
        int $code = 0,
        ?Throwable $previous = null,
        int $number = 0,
    )
    {
        parent::__construct($message, $code, $previous);

        $this->number = $number;
    }

    public function getNumber(): int
    {
        return $this->number;
    }
}
