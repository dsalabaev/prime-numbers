<?php

namespace App\Classes;

class PrimeNumber
{
    public int $number;
    public bool $is_prime;
    public ?int $next = null;
    public ?int $prev = null;
}
