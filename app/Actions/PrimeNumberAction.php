<?php

namespace App\Actions;

use App\Classes\PrimeNumber;
use App\Tasks\IsPrimeTask;
use App\Tasks\NextPrimeTask;
use App\Tasks\PrevPrimeTask;
use Illuminate\Support\Facades\Log;

class PrimeNumberAction
{
    public function run(int $number): ?PrimeNumber
    {
        try {
            $primeNumber = new PrimeNumber();
            $primeNumber->number = $number;

            $primeNumber->is_prime = app()->make(IsPrimeTask::class)->run($number);

            if (!$primeNumber->is_prime) {
                $primeNumber->prev = app()->make(PrevPrimeTask::class)->run($number - 1);
                $primeNumber->next = app()->make(NextPrimeTask::class)->run($number + 1);
            }

            return $primeNumber;
        } catch (\Throwable $throwable) {
            Log::error($throwable->getMessage(), $throwable->getTrace());

            return null;
        }
    }
}
