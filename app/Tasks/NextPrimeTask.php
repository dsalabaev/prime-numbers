<?php

namespace App\Tasks;

use App\Exceptions\InvalidPrimeNumberException;

readonly class NextPrimeTask
{
    const NEXT_PRIME_NUMBER_LIMIT = 1000000;

    public function __construct(
        private IsPrimeTask $isPrimeTask
    )
    {
    }

    /**
     * @throws InvalidPrimeNumberException
     */
    public function run(int $num): ?int
    {
        if ($num > self::NEXT_PRIME_NUMBER_LIMIT) return null;
        $isPrime = $this->isPrimeTask->run($num);

        if (!$isPrime) return $this->run($num + 1);

        return $num;
    }
}
