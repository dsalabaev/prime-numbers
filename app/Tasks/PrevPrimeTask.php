<?php

namespace App\Tasks;

use App\Exceptions\InvalidPrimeNumberException;

readonly class PrevPrimeTask
{
    public function __construct(
        private IsPrimeTask $isPrimeTask
    )
    {
    }

    /**
     * @throws InvalidPrimeNumberException
     */
    public function run(int $num): ?int
    {
        if ($num <= 1) return null;

        $isPrime = $this->isPrimeTask->run($num);

        if (!$isPrime) return $this->run($num - 1);

        return $num;
    }
}
