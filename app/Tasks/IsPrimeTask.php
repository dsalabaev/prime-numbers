<?php

namespace App\Tasks;

use App\Exceptions\InvalidPrimeNumberException;

class IsPrimeTask
{

    /**
     * @throws InvalidPrimeNumberException
     */
    public function run(int $num): bool
    {
        if ($num <= 1) {
            throw new InvalidPrimeNumberException(number: $num);
        }

        list($prime, $i, $d) = [
            ($num % 2 != 0 || $num == 2) and ($num % 3 != 0 or $num == 3),
            5,
            2
        ];

        while ($prime && $i * $i <= $num) {
            $prime = $num % $i != 0;
            $i += $d;
            $d = 6 - $d;
        }

        return $prime;
    }
}
