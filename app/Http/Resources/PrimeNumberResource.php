<?php

namespace App\Http\Resources;

use App\Classes\PrimeNumber;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PrimeNumber
 */
class PrimeNumberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'number' => $this->number,
            'is_prime' => $this->is_prime,
            'next' => $this->next,
            'prev' => $this->prev,
        ];
    }
}
