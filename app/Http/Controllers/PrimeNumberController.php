<?php

namespace App\Http\Controllers;

use App\Actions\PrimeNumberAction;
use App\Http\Requests\PrimeNumberRequest;
use App\Http\Resources\PrimeNumberResource;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class PrimeNumberController extends Controller
{
    public function index(): View|Factory|Application
    {
        return view('welcome');
    }

    public function check(PrimeNumberRequest $request): PrimeNumberResource|JsonResponse
    {
        $number = $request->validated('number');
        $primeNumber = app()->make(PrimeNumberAction::class)->run($number);

        if (empty($primeNumber)){
            return response()->json([
                'message' => 'app error'
            ], 500);
        }

        return PrimeNumberResource::make($primeNumber);
    }
}
