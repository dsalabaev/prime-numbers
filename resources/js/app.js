import './bootstrap';

const formHandler = () => {
    const form = document.getElementById('isPrimeForm');
    const result = document.getElementById('result');

    form.addEventListener('submit', event => {
        event.preventDefault();

        const input = event.target[0];
        if (!input || !input.value) return;

        window.axios.get('/check', {
            params: {
                number: input.value
            }
        }).then(res => {
            const {
                is_prime,
                number,
                next,
                prev,
            } = res.data.data;

            let html = `<p>Число ${number}: ${is_prime ? '':'НЕ '}простое число</p>`;

            if (prev) html += `<p>Близкое число (<): ${prev}</p>`
            if (next) html += `<p>Близкое число (>): ${next}</p>`

            result.innerHTML = html;
        })
    })
}

formHandler();
