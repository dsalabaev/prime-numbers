<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body>
    <form id="isPrimeForm">
        <label>
            <input type="number" name="prime_number" min="0" max="1000000" step="1" required>
        </label>
        <button form="isPrimeForm">
            Проверить
        </button>
    </form>
    <div id="result"></div>
    </body>
</html>
