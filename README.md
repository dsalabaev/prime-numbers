## Up project

### install vendor
    docker run --rm \
        -u "$(id -u):$(id -g)" \
        -v "$(pwd):/var/www/html" \
        -w /var/www/html \
        laravelsail/php83-composer:latest \
        composer install --ignore-platform-reqs

### install node_modules
    ./vendor/bin/sail npm install

### run
    cp .env.example .env
    ./vendor/bin/sail artisan key:generate
    ./vendor/bin/sail npm run dev
